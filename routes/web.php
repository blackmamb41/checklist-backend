<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function() {
    //Checklist App
    Route::get('/', 'Admin\HomeController@index');
    Route::get('/checklist/items', 'Admin\ItemController@index');
    Route::get('/checklist/items/add', 'Admin\ItemController@indexAddItem');
    Route::post('/checklist/items/add/create', 'Admin\ItemController@addItem');
    Route::get('/checklist/items/edit', 'Admin\ItemController@editItem');
    Route::post('/checklist/items/edit/save', 'Admin\ItemController@saveItem');
    Route::get('/checklist/items/delete', 'Admin\ItemController@deleteItem');
    Route::get('/checklist/users', 'Admin\UserController@index');
    Route::get('/checklist/users/add', 'Admin\UserController@indexAddUser');
    Route::post('/checklist/users/add/create', 'Admin\UserController@addUser');
    Route::get('/checklist/users/edit/{id}', 'Admin\UserController@editUser');
    Route::post('/checklist/users/edit/save/{id}', 'Admin\UserController@saveUser');
    Route::get('/checklist/users/delete', 'Admin\userController@deleteUser');
    Route::get('/checklist/reports', 'Admin\ResultController@index');
    Route::get('/checklist/reports/export', 'Admin\ResultController@exportToExcel');
    Route::get('/checklist/details/daily', 'Admin\ResultController@detailsReport');
    Route::get('/topologi/default', 'Admin\TopologiController@test');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
