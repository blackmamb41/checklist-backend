<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/summary', 'Apis\ReportController@summaryReport');
Route::get('/items', 'Apis\ItemController@notYetChecked');
Route::get('/items/checked', 'Apis\ItemController@listChecked');
Route::get('/items/single', 'Apis\ItemController@checkingSingleItem');
Route::post('/user/login', 'Apis\UserController@userLogin');
Route::post('/report/checking/insert', 'Apis\ReportController@doChecking');
Route::get('/report/send', 'Apis\ReportController@sendReport');
Route::get('/report/details', 'Apis\ReportController@prettyReport');
Route::get('/report/details/table', 'Apis\ReportController@forDetailsTable');
Route::get('/items/filter', 'Apis\ItemController@listSiteOrderByUser');