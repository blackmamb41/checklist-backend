<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Result;
use App\User;
use DB;

class ItemController extends Controller
{
    public function notYetChecked(Request $request)
    {
        $nik = $request->query('id_user');
        $site = $request->query('site');
        $floor = $request->query('floor');
        $today = date('Y/m/d');
        $userData = User::where('id', $nik)->first();
        $filterItem = Item::where('pvg_user', $nik)->where('site', $site)->where('floor', $floor)->get();
        $dataExist = DB::table('results')->where('check_user', $nik)->where('date', $today)->get();
        $existCodenameItem = array();
        foreach($dataExist as $item) {
            $existCodenameItem[] = $item->item_codename;
        }
        $dataItem = Item::where('pvg_user', $nik)->where('site', $site)->where('floor', $floor)->get();
        $codenameItem = array();
        foreach($dataItem as $item) {
            $codenameItem[] = $item->codename;
        }
        $toShowItemCodename = array_diff($codenameItem,$existCodenameItem);
        $result = DB::table('items')->whereIn('codename', $toShowItemCodename)->get();

        return response()->json($result);
    }

    public function listChecked(Request $request)
    {
        $nik = $request->query('id_user');
        $site = $request->query('site');
        $floor = $request->query('floor');
        $today = date('Y/m/d');
        $getData = DB::table('results')
                ->select(
                    'results.id as id',
                    'results.date as date',
                    'results.time as time',
                    'results.status as status',
                    'results.data as data',
                    'results.check_user as check_user',
                    'items.codename as codename',
                    'items.NE as NE',
                    'items.type as type',
                    'items.SN as SN',
                    'items.site as site',
                    'items.floor as floor',
                    'items.pvg_user as pvg_user'
                )
                ->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                ->where('check_user', $nik)
                ->where('date', $today)
                ->where('site', $site)
                ->where('floor', $floor)
                ->get();
        // $userData = User::where('id', $nik)->first();
        // $filterItem = Item::where('pvg_user', $nik)->get();
        // $dataExist = DB::table('results')->where('check_user', $nik)->where('date', $today)->get();

        return response()->json($getData);
    }

    public function checkingSingleItem(Request $request)
    {
        $codename = $request->query('codename');
        $data = Item::where('codename', $codename)->first();

        return response()->json($data);
    }

    public function listSiteOrderByUser(Request $request)
    {
        $nik = $request->query('id_user');
        $testData = DB::table('items')->select(DB::raw("site, floor"))->where('pvg_user', $nik)->groupBy('site', 'floor')->orderBy('site', 'asc')->orderBy('floor','asc')->get();
        return response()->json($testData);
    }
}
