<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DatePeriod;
use DateTime;
use DateInterval;
use DB;
use App\Models\Item;
use App\Models\Result;
use App\User;
use Telegram\Bot\Laravel\Facades\Telegram;
use Emojify;

class ReportController extends Controller
{
    public function summaryReport(Request $request)
    {
        $start = $request->query('start');
        $end = $request->query('end');
        $ts = $request->query('enginer');
        $endDate = new DateTime($end);
        $endDate->modify('+1 day');
        $period = new DatePeriod(
            new DateTime($start),
            new DateInterval('P1D'),
            $endDate
        );
        $getAllAssignment = Item::where('pvg_user', $ts)->get();
        $arrayAssignment = array();
        $arraySummary = array();
        $dateData = array();
        foreach($period as $key => $value){
            $dateData[$key] = $value->format('Y-m-d');
        }
        foreach($getAllAssignment as $valAssignment){
            $getOK = Result::whereIn('date', $dateData)->where('check_user', $ts)->where('item_codename', $valAssignment->codename)->where('status', 1)->get()->count();
            $getNOK = Result::whereIn('date', $dateData)->where('check_user', $ts)->where('item_codename', $valAssignment->codename)->where('status', 0)->get()->count();
            $arraySummary[] = (object) array('NE' => $valAssignment->NE, 'site' => $valAssignment->site, 'floor' => $valAssignment->floor, 'countOK' => $getOK, 'countNOK' => $getNOK);
            $arrayAssignment[] = $valAssignment->codename;
        }

        return response()->json($arraySummary);
    }

    public function prettyReport(Request $request)
    {
        $getDate = $request->query('tanggal');
        $filterDate = date('Y-m-d', strtotime($getDate));
        $getCountNOKSwitchSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKSwitchSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKSwitchSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();

    // UPS COUNTING
        $getCountNOKUPSSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ups')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKUPSSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKUPSSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();

    // AC COUNTING
        $getCountNOKACSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountNOKACSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();
        $getCountOKACSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $filterDate)
                            ->get()
                            ->count();

    //ROUTER COUNTING
        $getCountNOKRouterSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'rosuta')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'ttcsuta')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'tasik')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cirebon')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'windu')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cianjur')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountNOKRouterSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'soreang')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'rosuta')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'ttcsuta')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'tasik')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cirebon')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'windu')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cianjur')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();
        $getCountOKRouterSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'soreang')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $filterDate)
            ->get()
            ->count();

    // SERVER COUNTING
            $getCountNOKServerSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'rosuta')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'ttcsuta')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'tasik')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cirebon')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'windu')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cianjur')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountNOKServerSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'soreang')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'rosuta')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'ttcsuta')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'tasik')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cirebon')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'windu')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cianjur')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();
            $getCountOKServerSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'soreang')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $filterDate)
                        ->get()
                        ->count();

    // CCTV COUNTING
            $getCountNOKCCTVSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'rosuta')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'ttcsuta')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'tasik')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cirebon')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'windu')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cianjur')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountNOKCCTVSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'soreang')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'rosuta')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'ttcsuta')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'tasik')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cirebon')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'windu')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cianjur')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();
            $getCountOKCCTVSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'soreang')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $filterDate)
                                ->get()
                                ->count();

            $dataCollect = (object) [
                'scs'   => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACSCS, 'nok' => $getCountNOKACSCS),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVSCS, 'nok' => $getCountNOKCCTVSCS),
                    'router'=> (object) array('ok' => $getCountOKRouterSCS, 'nok' => $getCountNOKRouterSCS),
                    'server'=> (object) array('ok' => $getCountOKServerSCS, 'nok' => $getCountNOKServerSCS),
                    'switch'=> (object) array('ok' => $getCountOKSwitchSCS, 'nok' => $getCountNOKSwitchSCS),
                    'ups'   => (object) array('ok' => $getCountOKUPSSCS, 'nok' => $getCountNOKUPSSCS)
                ],
                'ttc'   => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACTTC, 'nok' => $getCountNOKACTTC),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVTTC, 'nok' => $getCountNOKCCTVTTC),
                    'router'=> (object) array('ok' => $getCountOKRouterTTC, 'nok' => $getCountNOKRouterTTC),
                    'server'=> (object) array('ok' => $getCountOKServerTTC, 'nok' => $getCountNOKServerTTC),
                    'switch'=> (object) array('ok' => $getCountOKSwitchTTC, 'nok' => $getCountNOKSwitchTTC),
                    'ups'   => (object) array('ok' => $getCountOKUPSTTC, 'nok' => $getCountNOKUPSTTC)
                ],
                'tasik' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACTasik, 'nok' => $getCountNOKACTasik),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVTasik, 'nok' => $getCountNOKCCTVTasik),
                    'router'=> (object) array('ok' => $getCountOKRouterTasik, 'nok' => $getCountNOKRouterTasik),
                    'server'=> (object) array('ok' => $getCountOKServerTasik, 'nok' => $getCountNOKServerTasik),
                    'switch'=> (object) array('ok' => $getCountOKSwitchTasik, 'nok' => $getCountNOKSwitchTasik),
                    'ups'   => (object) array('ok' => $getCountOKUPSTasik, 'nok' => $getCountNOKUPSTasik)
                ],
                'cirebon' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACCirebon, 'nok' => $getCountNOKACCirebon),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVCirebon, 'nok' => $getCountNOKCCTVCirebon),
                    'router'=> (object) array('ok' => $getCountOKRouterCirebon, 'nok' => $getCountNOKRouterCirebon),
                    'server'=> (object) array('ok' => $getCountOKServerCirebon, 'nok' => $getCountNOKServerCirebon),
                    'switch'=> (object) array('ok' => $getCountOKSwitchCirebon, 'nok' => $getCountNOKSwitchCirebon),
                    'ups'   => (object) array('ok' => $getCountOKUPSCirebon, 'nok' => $getCountNOKUPSCirebon)
                ],
                'cianjur' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACCianjur, 'nok' => $getCountNOKACCianjur),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVCianjur, 'nok' => $getCountNOKCCTVCianjur),
                    'router'=> (object) array('ok' => $getCountOKRouterCianjur, 'nok' => $getCountNOKRouterCianjur),
                    'server'=> (object) array('ok' => $getCountOKServerCianjur, 'nok' => $getCountNOKServerCianjur),
                    'switch'=> (object) array('ok' => $getCountOKSwitchCianjur, 'nok' => $getCountNOKSwitchCianjur),
                    'ups'   => (object) array('ok' => $getCountOKUPSCianjur, 'nok' => $getCountNOKUPSCianjur)
                ],
                'soreang' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACSoreang, 'nok' => $getCountNOKACSoreang),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVSoreang, 'nok' => $getCountNOKCCTVSoreang),
                    'router'=> (object) array('ok' => $getCountOKRouterSoreang, 'nok' => $getCountNOKRouterSoreang),
                    'server'=> (object) array('ok' => $getCountOKServerSoreang, 'nok' => $getCountNOKServerSoreang),
                    'switch'=> (object) array('ok' => $getCountOKSwitchSoreang, 'nok' => $getCountNOKSwitchSoreang),
                    'ups'   => (object) array('ok' => $getCountOKUPSSoreang, 'nok' => $getCountNOKUPSSoreang)
                ],
                'windu' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACWindu, 'nok' => $getCountNOKACWindu),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVWindu, 'nok' => $getCountNOKCCTVWindu),
                    'router'=> (object) array('ok' => $getCountOKRouterWindu, 'nok' => $getCountNOKRouterWindu),
                    'server'=> (object) array('ok' => $getCountOKServerWindu, 'nok' => $getCountNOKServerWindu),
                    'switch'=> (object) array('ok' => $getCountOKSwitchWindu, 'nok' => $getCountNOKSwitchWindu),
                    'ups'   => (object) array('ok' => $getCountOKUPSWindu, 'nok' => $getCountNOKUPSWindu)
                ]
            ];

        return response()->json(['countData' => $dataCollect]);
    }

    public function forDetailsTable(Request $request)
    {
        $filterDate = $request->query('date');
        $filterType = $request->query('type');
        $filterSite = $request->query('site');
        $convertDate = date('Y-m-d', strtotime($filterDate));
        $getData = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                ->where('type', $filterType)
                ->where('site', $filterSite)
                ->where('date', $convertDate)
                ->get();
        $realData = array();
        foreach($getData as $response){
            $realData[] = [
                'id'        => $response->id,
                'codename'  => $response->codename,
                'ne'        => $response->NE,
                'type'      => $response->type,
                'site'      => $response->site,
                'date'      => $response->date,
                'status'    => $response->status,
                'data'      => unserialize($response->data)
            ];
        }

        return response()->json($realData);
    }

    public function doChecking(Request $request)
    {
        $today = date('Y/m/d');
        $codename = $request->query('codename');
        $time = $request->time;
        $status = $request->status;
        $data = serialize($request->data);
        $user = $request->enginer;
        Result::create([
            'item_codename' => $codename,
            'date'          => $today,
            'time'          => $time,
            'status'        => $status,
            'approve'       => 0,
            'data'          => $data,
            'check_user'    => $user,
            'approve_user'  => 0
        ]);

        return response()->json(['msg' => 'Success']);
    }

    public function sendReport(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $user = $request->query('id_user');
        $site = $request->query('site');
        $floor = $request->query('floor');
        $today = date('Y/m/d');
        $timeNow = date('H:m:s');
        $getData = DB::table('results')
                ->select(
                    'results.id as id',
                    'results.date as date',
                    'results.time as time',
                    'results.status as status',
                    'results.data as data',
                    'results.check_user as check_user',
                    'items.codename as codename',
                    'items.NE as NE',
                    'items.type as type',
                    'items.SN as SN',
                    'items.site as site',
                    'items.floor as floor',
                    'items.pvg_user as pvg_user'
                )
                ->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                ->where('check_user', $user)
                ->where('site', $site)
                ->where('floor', $floor)
                ->where('date', $today)
                ->get();
        $getUserName = User::where('id', $user)->first();
        $listItemChecked = [];
        $showSite = [];
        foreach($getData as $items){
            if($items->status == 1){
                $status = 'OK';
            }else{
                $status = 'NOK';
            }
            $listItemChecked[] = $items->NE . '  -  ' . '<b>' . $status . '</b>';
            $showSite[] = $items->site;
        }
        $dataToShowReport = implode("\n", $listItemChecked);
        $siteMarge = implode("",array_unique($showSite));
        if($siteMarge == 'rosuta'){
            $siteName = 'SCS Regional Jabar';
        }elseif($siteMarge == 'ttcsuta'){
            $siteName = 'TTC Regional Jabar';
        }elseif($siteMarge == 'tasik'){
            $siteName = 'Branch Tasikmalaya';
        }elseif($siteMarge == 'cirebon'){
            $siteName = 'Branch Cirebon';
        }elseif($siteMarge == 'soreang'){
            $siteName = 'Branch Soreang';
        }elseif($siteMarge == 'cianjur'){
            $siteName = 'Grapari Cianjur';
        }elseif($siteMarge == 'windu'){
            $siteName = 'Branch bandung';
        }elseif($siteMarge == 'grapdago'){
            $siteName = 'Grapari Dago';
        }

        $format = "<b>RESULT REPORT</b> \n"
                . "Enginer : " . "$getUserName->name" . "\n"
                . "Date : " . "$today" . "-" . "$timeNow" . "\n"
                . "Site : " . "$siteName" . " - Lantai : " . $floor . "\n"
                . "\n"
                . $dataToShowReport;

        Telegram::sendMessage([
            // 'chat_id'       => env('TELEGRAM_CHANNEL_ID', '-334452977'), 
            'chat_id'       => env('TELEGRAM_CHANNEL_ID', '-320037546'),
            'parse_mode'    => 'HTML',
            'text'          => $format
        ]);

        // $getIdChat = Telegram::getUpdates();
        
        return response()->json(['message' => 'Report Send']);
    }
}
