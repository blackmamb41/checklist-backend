<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function userLogin(Request $request)
    {
        $nik = $request->nik;
        $dataUser = User::where('nik', $nik)->first();
        $pass = $request->password;
        $verify = Hash::check($pass, $dataUser->password);
        if($verify == false) {
            return response()->json(['msg' => 'Unauthorized'], 401);
        }else{
            return response()->json($dataUser);
        }
    }
}
