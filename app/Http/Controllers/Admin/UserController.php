<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('userindex', ['data' => $data]);
    }

    public function indexAddUser()
    {
        return view('adduser');
    }

    public function addUser(Request $request)
    {
        User::create([
            'name'  => $request->name,
            'nik'   => $request->nik,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role'  => $request->role
        ]);

        return redirect('/checklist/users')->with('message', 'Adding Success');
    }

    public function editUser(Request $request, $id)
    {
        $getUser = User::where('id', $id)->first();
        $role = array("admin", "enginer");

        return view('edituser', ['user' => $getUser, 'role' => $role]);
    }

    public function saveUser(Request $request, $id)
    {
        $updateUser = User::where('id', $id)
                    ->update(array_filter($request->except('_token')));
        if($request->password == null){
            return redirect('/checklist/users');
        }else{
            User::where('id', $id)->update(['password' => Hash::make($request->password)]);
            return redirect('/checklist/users');
        }
    }

    public function deleteUser(Request $request)
    {
        $getId = $request->query('id');
        User::where('id', $getId)->delete();

        return redirect('/checklist/users')->with('message', 'User has Deleted');
    }
}
