<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReportExport;

class ResultController extends Controller
{
    
    public function index()
    {
        $enginer = User::where('role', 'enginer')->get();
        return view('report', ['data' => $enginer]);
    }

    protected $start;
    public function exportToExcel(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $timeNow = date("H:i:s");
        $dateToday = date("d-m-Y");
        $start = $request->query('start');
        $end = $request->query('end');
        $enginer = $request->query('enginer');
        $filter = (object) array('startDate' => $start, 'endDate' => $end, 'ts' => $enginer);
        return (new ReportExport($filter))->download('Report-' . $dateToday . ' ' . $timeNow . '.xlsx');
    }

    public function detailsReport(Request $request)
    {
        $today = date('Y-m-d');
        $getUserTS = User::where('role', 'enginer')->get();
    
    // SWITCH COUNTING
        $getCountNOKSwitchSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKSwitchSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKSwitchSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'switch')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();

    // UPS COUNTING
        $getCountNOKUPSSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ups')
                            ->where('type', 'switch')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKUPSSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ups')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKUPSSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ups')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();

    // AC COUNTING
        $getCountNOKACSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountNOKACSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ac')
                            ->where('status', 0)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'rosuta')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'ttcsuta')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'tasik')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cirebon')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'windu')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'cianjur')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();
        $getCountOKACSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                            ->where('site', 'soreang')
                            ->where('type', 'ac')
                            ->where('status', 1)
                            ->where('date', $today)
                            ->get()
                            ->count();

    //ROUTER COUNTING
        $getCountNOKRouterSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'rosuta')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'ttcsuta')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'tasik')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cirebon')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'windu')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cianjur')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountNOKRouterSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'soreang')
            ->where('type', 'router')
            ->where('status', 0)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'rosuta')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'ttcsuta')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'tasik')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cirebon')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'windu')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'cianjur')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();
        $getCountOKRouterSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
            ->where('site', 'soreang')
            ->where('type', 'router')
            ->where('status', 1)
            ->where('date', $today)
            ->get()
            ->count();

    // SERVER COUNTING
            $getCountNOKServerSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'rosuta')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'ttcsuta')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'tasik')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cirebon')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'windu')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cianjur')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountNOKServerSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'soreang')
                        ->where('type', 'server')
                        ->where('status', 0)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'rosuta')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'ttcsuta')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'tasik')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cirebon')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'windu')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'cianjur')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();
            $getCountOKServerSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                        ->where('site', 'soreang')
                        ->where('type', 'server')
                        ->where('status', 1)
                        ->where('date', $today)
                        ->get()
                        ->count();

    // CCTV COUNTING
            $getCountNOKCCTVSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'rosuta')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'ttcsuta')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'tasik')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cirebon')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'windu')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cianjur')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountNOKCCTVSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'soreang')
                                ->where('type', 'cctv')
                                ->where('status', 0)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVSCS = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'rosuta')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVTTC = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'ttcsuta')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVTasik = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'tasik')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVCirebon = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cirebon')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVWindu = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'windu')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVCianjur = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'cianjur')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();
            $getCountOKCCTVSoreang = DB::table('results')->leftJoin('items', 'results.item_codename', '=', 'items.codename')
                                ->where('site', 'soreang')
                                ->where('type', 'cctv')
                                ->where('status', 1)
                                ->where('date', $today)
                                ->get()
                                ->count();

            $dataCollect = (object) [
                'scs'   => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACSCS, 'nok' => $getCountNOKACSCS),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVSCS, 'nok' => $getCountNOKCCTVSCS),
                    'router'=> (object) array('ok' => $getCountOKRouterSCS, 'nok' => $getCountNOKRouterSCS),
                    'server'=> (object) array('ok' => $getCountOKServerSCS, 'nok' => $getCountNOKServerSCS),
                    'switch'=> (object) array('ok' => $getCountOKSwitchSCS, 'nok' => $getCountNOKSwitchSCS),
                    'ups'   => (object) array('ok' => $getCountOKUPSSCS, 'nok' => $getCountNOKUPSSCS)
                ],
                'ttc'   => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACTTC, 'nok' => $getCountNOKACTTC),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVTTC, 'nok' => $getCountNOKCCTVTTC),
                    'router'=> (object) array('ok' => $getCountOKRouterTTC, 'nok' => $getCountNOKRouterTTC),
                    'server'=> (object) array('ok' => $getCountOKServerTTC, 'nok' => $getCountNOKServerTTC),
                    'switch'=> (object) array('ok' => $getCountOKSwitchTTC, 'nok' => $getCountNOKSwitchTTC),
                    'ups'   => (object) array('ok' => $getCountOKUPSTTC, 'nok' => $getCountNOKUPSTTC)
                ],
                'tasik' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACTasik, 'nok' => $getCountNOKACTasik),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVTasik, 'nok' => $getCountNOKCCTVTasik),
                    'router'=> (object) array('ok' => $getCountOKRouterTasik, 'nok' => $getCountNOKRouterTasik),
                    'server'=> (object) array('ok' => $getCountOKServerTasik, 'nok' => $getCountNOKServerTasik),
                    'switch'=> (object) array('ok' => $getCountOKSwitchTasik, 'nok' => $getCountNOKSwitchTasik),
                    'ups'   => (object) array('ok' => $getCountOKUPSTasik, 'nok' => $getCountNOKUPSTasik)
                ],
                'cirebon' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACCirebon, 'nok' => $getCountNOKACCirebon),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVCirebon, 'nok' => $getCountNOKCCTVCirebon),
                    'router'=> (object) array('ok' => $getCountOKRouterCirebon, 'nok' => $getCountNOKRouterCirebon),
                    'server'=> (object) array('ok' => $getCountOKServerCirebon, 'nok' => $getCountNOKServerCirebon),
                    'switch'=> (object) array('ok' => $getCountOKSwitchCirebon, 'nok' => $getCountNOKSwitchCirebon),
                    'ups'   => (object) array('ok' => $getCountOKUPSCirebon, 'nok' => $getCountNOKUPSCirebon)
                ],
                'cianjur' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACCianjur, 'nok' => $getCountNOKACCianjur),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVCianjur, 'nok' => $getCountNOKCCTVCianjur),
                    'router'=> (object) array('ok' => $getCountOKRouterCianjur, 'nok' => $getCountNOKRouterCianjur),
                    'server'=> (object) array('ok' => $getCountOKServerCianjur, 'nok' => $getCountNOKServerCianjur),
                    'switch'=> (object) array('ok' => $getCountOKSwitchCianjur, 'nok' => $getCountNOKSwitchCianjur),
                    'ups'   => (object) array('ok' => $getCountOKUPSCianjur, 'nok' => $getCountNOKUPSCianjur)
                ],
                'soreang' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACSoreang, 'nok' => $getCountNOKACSoreang),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVSoreang, 'nok' => $getCountNOKCCTVSoreang),
                    'router'=> (object) array('ok' => $getCountOKRouterSoreang, 'nok' => $getCountNOKRouterSoreang),
                    'server'=> (object) array('ok' => $getCountOKServerSoreang, 'nok' => $getCountNOKServerSoreang),
                    'switch'=> (object) array('ok' => $getCountOKSwitchSoreang, 'nok' => $getCountNOKSwitchSoreang),
                    'ups'   => (object) array('ok' => $getCountOKUPSSoreang, 'nok' => $getCountNOKUPSSoreang)
                ],
                'windu' => (object) [
                    'ac'    => (object) array('ok' => $getCountOKACWindu, 'nok' => $getCountNOKACWindu),
                    'cctv'  => (object) array('ok' => $getCountOKCCTVWindu, 'nok' => $getCountNOKCCTVWindu),
                    'router'=> (object) array('ok' => $getCountOKRouterWindu, 'nok' => $getCountNOKRouterWindu),
                    'server'=> (object) array('ok' => $getCountOKServerWindu, 'nok' => $getCountNOKServerWindu),
                    'switch'=> (object) array('ok' => $getCountOKSwitchWindu, 'nok' => $getCountNOKSwitchWindu),
                    'ups'   => (object) array('ok' => $getCountOKUPSWindu, 'nok' => $getCountNOKUPSWindu)
                ]
            ];
        return view('details', ['data' => $dataCollect]);
    }

}
