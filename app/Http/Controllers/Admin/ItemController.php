<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use Uuid;
use App\User;

class ItemController extends Controller
{
    public function index()
    {
        $getAll = Item::all();

        return view('itemindex', ['data' => $getAll]);
    }

    public function indexAddItem()
    {
        $ts = User::where('role', 'enginer')->get();
        return view('additem', ['user' => $ts]);
    }

    public function addItem(Request $request)
    {
        Item::create([
            'codename'  => Uuid::generate(4)->string,
            'ne'        => $request->ne,
            'type'      => $request->type,
            'sn'        => $request->sn,
            'site'      => $request->site,
            'floor'     => $request->floor,
            'pvg_user'  => $request->enginer
        ]);

        return redirect('/checklist/items')->with('message', 'NE Success Added');
    }

    public function editItem(Request $request)
    {
        $codename = $request->query('codename');
        $getItem = Item::where('codename', $codename)->first();
        $ts = User::where('role', 'enginer')->get();
        $site = array('rosuta', 'ttcsuta', 'cirebon', 'tasik', 'cianjur', 'windu', 'soreang');
        $type = ['ups', 'switch', 'router', 'server', 'ac', 'cctv', 'wireless'];
        $floor = array(1, 2, 3, 4, 5, 6);

        return view('edititem', ['data' => $getItem, 'user' => $ts, 'type' => $type, 'site' => $site, 'floor' => $floor]);
    }

    public function saveItem(Request $request)
    {
        $codename = $request->query('codename');
        $updateItem = Item::where('codename', $codename)
                    ->update([
                        'ne'        => $request->ne,
                        'type'      => $request->type,
                        'sn'        => $request->sn,
                        'site'      => $request->site,
                        'floor'     => $request->floor,
                        'pvg_user'  => $request->enginer
                    ]);
        
        return redirect('/checklist/items')->with('message', 'Edit Item Successfully');
    }

    public function deleteItem(Request $request)
    {
        $codename = $request->query('codename');
        Item::where('codename', $codename)->delete();

        return redirect('/checklist/items')->with('message', 'Item has Deleted');
    }
}
