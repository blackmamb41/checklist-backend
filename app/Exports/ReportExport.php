<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Result;
use Illuminate\Http\Request;
use App\Models\Item;
use DatePeriod;
use DateTime;
use DateInterval;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Http\Admin\ResultController;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\AfterSheet;

class ReportExport implements FromView, WithDrawings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Result::all();
    // }
    use Exportable;

    // protected $start;
    // protected $end;
    // protected $enginer;
    public function __construct($start)
    {
        $this->filter = $start;
    }

    public function drawings()
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Logo');
        $drawing->setPath(public_path('/img/telkomsel.png'));
        $drawing->setHeight(90);

        return $drawing;
    }

    public function view(): View
    {
        $endDate = new DateTime($this->filter->endDate);
        $endDate->modify('+1 day');
        $period = new DatePeriod(
            new DateTime($this->filter->startDate),
            new DateInterval('P1D'),
            $endDate
        );
        $getAllAssignment = Item::where('pvg_user', $this->filter->ts)->get();
        $arrayAssignment = array();
        $arraySummary = array();
        $dateData = array();
        foreach($period as $key => $value){
            $dateData[$key] = $value->format('Y-m-d');
        }
        foreach($getAllAssignment as $valAssignment){
            $getOK = Result::whereIn('date', $dateData)->where('check_user', $this->filter->ts)->where('item_codename', $valAssignment->codename)->where('status', 1)->get()->count();
            $getNOK = Result::whereIn('date', $dateData)->where('check_user', $this->filter->ts)->where('item_codename', $valAssignment->codename)->where('status', 0)->get()->count();
            $arraySummary[] = (object) array('NE' => $valAssignment->NE, 'site' => $valAssignment->site, 'floor' => $valAssignment->floor, 'countOK' => $getOK, 'countNOK' => $getNOK);
            $arrayAssignment[] = $valAssignment->codename;
        }

        return view('tablereport', [
            'report'    => $arraySummary,
            'startdate' => $this->filter->startDate,
            'enddate'   => $endDate,
            'enginer'   => $this->filter->ts
        ]);
    }
}
