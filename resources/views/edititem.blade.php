@extends('core')

@section('styles')
@endsection

@section('title')
    Edit - {{$data->NE}}
@endsection

@section('body')
    <div class="row justify-content-md-center">
        <section class="content-header">
            <h1>
                Edit NE Item
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <form action="/checklist/items/edit/save?codename={{$data->codename}}" method="POST">
                    @csrf
                    <div class="col-md-8">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">NE Details</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name"><i class="fa fa-certificate" style="color: red;"> </i>Hardware</label>
                                    <input type="text" class="form-control" id="ne" name="ne" placeholder="Hardware Name" required value="{{$data->NE}}"/>
                                </div>
                                <div class="form-group">
                                    <label for="serial">Serial Number :</label>
                                    <input type="text" class="form-control" id="sn" name="sn" placeholder="Serial Number Hardware" value="{{$data->SN}}" />
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type"><i class="fa fa-certificate" style="color: red;"> </i>Type :</label>
                                        <select name="type" id="type" required class="form-control">
                                            <option value="">Choose Type..</option>
                                            @foreach($type as $typeItem)
                                                @if($data->type == $typeItem)
                                                    <option value="{{$typeItem}}" selected>
                                                        @if($typeItem == 'ups')
                                                            UPS
                                                        @elseif($typeItem == 'switch')
                                                            Switch
                                                        @elseif($typeItem == 'ac')
                                                            Air Conditioner
                                                        @elseif($typeItem == 'router')
                                                            Router
                                                        @elseif($typeItem == 'server')
                                                            Server
                                                        @elseif($typeItem == 'cctv')
                                                            CCTV
                                                        @elseif($typeItem == 'wireless')
                                                            Wifi
                                                        @endif
                                                    </option>
                                                @else
                                                    <option value="{{$typeItem}}">
                                                        @if($typeItem == 'ups')
                                                            UPS
                                                        @elseif($typeItem == 'switch')
                                                            Switch
                                                        @elseif($typeItem == 'ac')
                                                            Air Conditioner
                                                        @elseif($typeItem == 'router')
                                                            Router
                                                        @elseif($typeItem == 'server')
                                                            Server
                                                        @elseif($typeItem == 'cctv')
                                                            CCTV
                                                        @elseif($typeItem == 'wireless')
                                                            Wifi
                                                        @endif
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ts"><i class="fa fa-certificate" style="color: red;"> </i>Assignment :</label>
                                        <select name="enginer" id="enginer" class="form-control" required>
                                            <option value="">Choose TS...</option>
                                            @foreach($user as $ts)
                                                @if($data->pvg_user == $ts->id)
                                                    <option value="{{ $ts->id }}" selected>{{ $ts->name }}</option>
                                                @else
                                                    <option value="{{ $ts->id }}">{{ $ts->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="site"><i class="fa fa-certificate" style="color: red;"> </i>Site :</label>
                                        <select name="site" id="site" class="form-control" required>
                                        <option value="">Choose Site.....</option>
                                            @foreach($site as $sites)
                                                @if($data->site == $sites)
                                                    <option value="{{$sites}}" selected>
                                                        @if($sites == 'rosuta')
                                                            Regional Office Jabar
                                                        @elseif($sites == 'ttcsuta')
                                                            TTC Regional Jabar
                                                        @elseif($sites == 'cirebon')
                                                            Branch Cirebon
                                                        @elseif($sites == 'tasik')
                                                            Branch Tasikmalaya
                                                        @elseif($sites == 'cianjur')
                                                            Grapari Cianjur
                                                        @elseif($sites == 'windu')
                                                            Branch Windu
                                                        @elseif($sites == 'soreang')
                                                            Branch Soreang
                                                        @elseif($sites == 'grapdago')
                                                            Grapari Dago
                                                        @endif
                                                    </option>
                                                @else
                                                    <option value="{{$sites}}">
                                                        @if($sites == 'rosuta')
                                                            Regional Office Jabar
                                                        @elseif($sites == 'ttcsuta')
                                                            TTC Regional Jabar
                                                        @elseif($sites == 'cirebon')
                                                            Branch Cirebon
                                                        @elseif($sites == 'tasik')
                                                            Branch Tasikmalaya
                                                        @elseif($sites == 'cianjur')
                                                            Grapari Cianjur
                                                        @elseif($sites == 'windu')
                                                            Branch Windu
                                                        @elseif($sites == 'soreang')
                                                            Branch Soreang
                                                        @elseif($sites == 'grapdago')
                                                            Grapari Dago
                                                        @endif
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="floor"><i class="fa fa-certificate" style="color: red;"> </i>Floor :</label>
                                        <select name="floor" id="floor" class="form-control" required>
                                            <option value="">Choose Floor....</option>
                                            @foreach($floor as $floors)
                                                @if($data->floor == $floors)
                                                    <option value="{{$floors}}" selected>{{$floors}}</option>
                                                @else
                                                    <option value="{{$floors}}">{{$floors}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-block btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
