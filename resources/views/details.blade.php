@extends('core')

@section('title', 'Daily Report Summary')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/mycustom.css')}}">
@endsection

@section('body')
    <section class="content-header">
        <h1>
            <i class="fa fa-book"></i> Daily Report Summary
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3>Filter :</h3>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="date">Date :</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datefilter"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label for=""></label>
                                <button class="btn btn-block btn-primary" id="search"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="bodycontent">
                        <table id="reportstable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="text-align:center;justify-content:center;background-color: #3c8dbc;color:#fff">Sites</th>
                                    <th rowspan="2" style="text-align:center;justify-content:center;background-color: #3c8dbc;color:#fff">PIC</th>
                                    <th colspan="7" style="text-align:center;justify-content:center;background-color: #3c8dbc;color:#fff">Network Elements</th>
                                </tr>
                                <tr>
                                    <th scope="col" style="text-align:center;background-color: #3c8dbc;color:#fff">AC</th>
                                    <th scope="col" style="text-align:center;background-color: #3c8dbc;color:#fff">CCTV</th>
                                    <th scope="col" style="text-align:center;background-color:#3c8dbc;color:#fff">Router</th>
                                    <th scope="col" style="text-align:center;background-color: #3c8dbc;color:#fff">Server</th>
                                    <th scope="col" style="text-align:center;background-color: #3c8dbc;color:#fff">Switch</th>
                                    <th scope="col" style="text-align:center;background-color: #3c8dbc;color:#fff">UPS</th>
                                </tr>
                            </thead>
                            <tbody id="bodytable">
                                <tr>
                                    <td>Regional SCS Jabar</td>
                                    <td style="text-align:center">Subiyantoro</td>
                                    @if($data->scs->ac->ok == 0 && $data->scs->ac->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->ac->ok > 0 && $data->scs->ac->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->ac->ok > 0 && $data->scs->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->ac->nok}} Problem</div></a></td>
                                    @elseif($data->scs->ac->ok == 0 && $data->scs->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->ac->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->scs->cctv->ok == 0 && $data->scs->cctv->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->cctv->ok > 0 && $data->scs->cctv->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="cctvscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->cctv->ok > 0 && $data->scs->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->cctv->nok}} Problem</div></a></td>
                                    @elseif($data->scs->cctv->ok == 0 && $data->scs->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="cctvscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->cctv->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->scs->router->ok == 0 && $data->scs->router->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->router->ok > 0 && $data->scs->router->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="routerscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->router->ok == 0 && $data->scs->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="routerscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->router->nok}} Problem</div></a></td>
                                    @elseif($data->scs->router->ok > 0 && $data->scs->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->router->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->scs->server->ok == 0 && $data->scs->server->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->server->ok > 0 && $data->scs->server->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="serverscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->server->ok == 0 && $data->scs->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="serverscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->server->nok}} Problem</div></a></td>
                                    @elseif($data->scs->server->ok > 0 && $data->scs->serve->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->server->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->scs->switch->ok == 0 && $data->scs->switch->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->switch->ok > 0 && $data->scs->switch->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="switchscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->switch->ok == 0 && $data->scs->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="switchscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->switch->nok}} Problem</div></a></td>
                                    @elseif($data->scs->switch->ok > 0 && $data->scs->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->switch->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->scs->ups->ok == 0 && $data->scs->ups->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->scs->ups->ok > 0 && $data->scs->ups->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="upsscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->scs->ups->ok == 0 && $data->scs->ups->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="upsscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->ups->nok}} Problem</div></a></td>
                                    @elseif($data->scs->ups->ok > 0 && $data->scs->ups->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->scs->ups->nok}} Problem</div></a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>TTC Soetta Jabar</td>
                                    <td style="text-align:center">Teja Yuda Pratama</td>
                                    @if($data->ttc->ac->ok == 0 && $data->ttc->ac->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->ac->ok > 0 && $data->ttc->ac->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->ac->ok == 0 && $data->ttc->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->ac->nok}} Problem</div></a></td>
                                    @elseif($data->ttc->ac->ok > 0 && $data->ttc->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->ac->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->ttc->cctv->ok == 0 && $data->ttc->cctv->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->cctv->ok > 0 && $data->ttc->cctv->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="cctvttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->cctv->ok == 0 && $data->ttc->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="cctvttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->cctv->nok}} Problem</div></a></td>
                                    @elseif($data->ttc->cctv->ok > 0 && $data->ttc->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->cctv->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->ttc->router->ok == 0 && $data->ttc->router->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->router->ok > 0 && $data->ttc->router->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="routerttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->router->ok == 0 && $data->ttc->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="routerttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->router->nok}} Problem</div></a></td>
                                    @elseif($data->ttc->router->ok > 0 && $data->ttc->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->router->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->ttc->server->ok == 0 && $data->ttc->server->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->server->ok > 0 && $data->ttc->server->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="serverttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->server->ok == 0 && $data->ttc->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="serverttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->server->nok}} Problem</div></a></td>
                                    @elseif($data->ttc->server->ok > 0 && $data->ttc->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->server->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->ttc->switch->ok == 0 && $data->ttc->switch->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->switch->ok > 0 && $data->ttc->switch->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="switchttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->switch->ok == 0 && $data->ttc->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="switchttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->switch->nok}} Problem</div></a></td>
                                        @elseif($data->ttc->switch->ok > 0 && $data->ttc->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->switch->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->ttc->ups->ok == 0 && $data->ttc->ups->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->ttc->ups->ok > 0 && $data->ttc->ups->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="upsttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->ttc->ups->ok == 0 && $data->ttc->cctv->ups > 0)
                                        <td style="background-color:red"><a href="#"><div id="upsttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->ups->nok}} Problem</div></a></td>
                                    @elseif($data->ttc->ups->ok > 0 && $data->ttc->ups->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->ttc->ups->nok}} Problem</div></a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Branch Tasikmalaya</td>
                                    <td style="text-align:center">Asep Eriyanto</td>
                                    @if($data->tasik->ac->ok == 0 && $data->tasik->ac->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->ac->ok > 0 && $data->tasik->ac->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->ac->ok == 0 && $data->tasik->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->ac->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->ac->ok > 0 && $data->tasik->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->ac->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->tasik->cctv->ok == 0 && $data->tasik->cctv->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->cctv->ok > 0 && $data->tasik->cctv->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="cctvtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->cctv->ok == 0 && $data->tasik->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="cctvtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->cctv->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->cctv->ok > 0 && $data->tasik->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->cctv->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->tasik->router->ok == 0 && $data->tasik->router->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->router->ok > 0 && $data->tasik->router->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="routertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->router->ok == 0 && $data->tasik->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="routertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->router->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->router->ok > 0 && $data->tasik->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->router->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->tasik->server->ok == 0 && $data->tasik->server->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->server->ok > 0 && $data->tasik->server->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="servertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->server->ok == 0 && $data->tasik->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="servertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->server->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->server->ok > 0 && $data->tasik->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->server->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->tasik->switch->ok == 0 && $data->tasik->switch->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->switch->ok > 0 && $data->tasik->switch->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="switchtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->switch->ok == 0 && $data->tasik->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="switchtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->switch->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->switch->ok > 0 && $data->tasik->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->switch->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->tasik->ups->ok == 0 && $data->tasik->ups->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->tasik->ups->ok > 0 && $data->tasik->ups->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="upstasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->tasik->ups->ok == 0 && $data->tasik->cctv->ups > 0)
                                        <td style="background-color:red"><a href="#"><div id="upstasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->ups->nok}} Problem</div></a></td>
                                    @elseif($data->tasik->ups->ok > 0 && $data->tasik->ups->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->tasik->ups->nok}} Problem</div></a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Branch Cirebon</td>
                                    <td style="text-align:center">Agus Nugraha</td>
                                    @if($data->cirebon->ac->ok == 0 && $data->cirebon->ac->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->ac->ok > 0 && $data->cirebon->ac->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="accirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->ac->ok == 0 && $data->cirebon->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="accirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->ac->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->ac->ok > 0 && $data->cirebon->ac->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->ac->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->cirebon->cctv->ok == 0 && $data->cirebon->cctv->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->cctv->ok > 0 && $data->cirebon->cctv->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="cctvcirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->cctv->ok == 0 && $data->cirebon->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="cctvcirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->cctv->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->cctv->ok > 0 && $data->cirebon->cctv->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->cctv->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->cirebon->router->ok == 0 && $data->cirebon->router->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->router->ok > 0 && $data->cirebon->router->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="routercirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->router->ok == 0 && $data->cirebon->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="routercirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->router->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->router->ok > 0 && $data->cirebon->router->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->router->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->cirebon->server->ok == 0 && $data->cirebon->server->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->server->ok > 0 && $data->cirebon->server->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="servercirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->server->ok == 0 && $data->cirebon->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="servercirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->server->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->server->ok > 0 && $data->cirebon->server->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->server->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->cirebon->switch->ok == 0 && $data->cirebon->switch->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->switch->ok > 0 && $data->cirebon->switch->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="switchcirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->switch->ok == 0 && $data->cirebon->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="switchcirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->switch->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->switch->ok > 0 && $data->cirebon->switch->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->switch->nok}} Problem</div></a></td>
                                    @endif
                                    @if($data->cirebon->ups->ok == 0 && $data->cirebon->ups->nok == 0)
                                        <td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>
                                    @elseif($data->cirebon->ups->ok > 0 && $data->cirebon->ups->nok == 0)
                                        <td style="background-color:green"><a href="#"><div id="upscirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>
                                    @elseif($data->cirebon->ups->ok == 0 && $data->cirebon->cctv->ups > 0)
                                        <td style="background-color:red"><a href="#"><div id="upscirebon" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->ups->nok}} Problem</div></a></td>
                                    @elseif($data->cirebon->ups->ok > 0 && $data->cirebon->ups->nok > 0)
                                        <td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">{{$data->cirebon->ups->nok}} Problem</div></a></td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal fade" tabindex="-1" role="dialog" id="showdata">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Show Data</h4>
                                    </div>
                                    <div class="modal-body" id="datatabeldetails">
                                        <table id="tabledetails" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>NE</th>
                                                    <th>Humanity</th>
                                                    <th>Temperature</th>
                                                    <th>Info</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>AC Panasonic</th>
                                                    <th>20&#176; C</th>
                                                    <th>21&#176; C</th>
                                                    <th>Mati 1</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script>
    $(function() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        $("#datefilter").datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy',
            endDate: today
        })
        $("#datefilter").val(today);
    })
    $('#reportstable').dataTable({
        "lengthChange": false,
        "ordering": false,
        "searching": false,
        "columnDefs": [{
            "targets": [2,3,4,5,6,7],
            "orderable": false,
            "searchable": false,
            "width": 60
        }]
    });
    $('#search').on('click', function(e) {
        var filterDate = $('#datefilter').val()
        $('#bodytable').empty();
        $('#bodytable').append('<div class="spinner" id="initialstart"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>')
        $.get('/api/report/details?tanggal='+filterDate, function(data, error) {
            if(data.countData.scs.ac.ok == 0 && data.countData.scs.ac.nok == 0){
                var columnSCSAC = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.ac.ok > 0 && data.countData.scs.ac.nok == 0){
                var columnSCSAC = '<td style="background-color:green"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.scs.ac.ok == 0 && data.countData.scs.ac.nok > 0){
                var columnSCSAC = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.ac.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.ac.ok > 0 && data.countData.scs.ac.nok > 0){
                var columnSCSAC = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.ac.nok+' Problem</div></a></td>'
            }
            if(data.countData.scs.cctv.ok == 0 && data.countData.scs.cctv.nok == 0){
                var columnSCSCCTV = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.cctv.ok > 0 && data.countData.scs.cctv.nok == 0){
                var columnSCSCCTV = '<td style="background-color:green"><a href="#"><div id="cctvscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.scs.cctv.ok == 0 && data.countData.scs.cctv.nok > 0){
                var columnSCSCCTV = '<td style="background-color:red"><a href="#"><div id="cctvscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.cctv.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.cct.ok > 0 && data.countData.scs.cctv.nok > 0){
                var columnSCSCCTV = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.cctv.nok+' Problem</div></a></td>'
            }
            if(data.countData.scs.router.ok == 0 && data.countData.scs.router.nok == 0){
                var columnSCSRouter = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.router.ok > 0 && data.countData.scs.router.nok == 0){
                var columnSCSRouter = '<td style="background-color:green"><a href="#"><div id="routerscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.scs.router.ok == 0 && data.countData.scs.router.nok > 0){
                var columnSCSRouter = '<td style="background-color:red"><a href="#"><div id="routerscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.router.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.router.ok > 0 && data.countData.scs.router.nok > 0){
                var columnSCSRouter = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.router.nok+' Problem</div></a></td>'
            }
            if(data.countData.scs.server.ok == 0 && data.countData.scs.server.nok == 0){
                var columnSCSServer = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.server.ok > 0 && data.countData.scs.server.nok == 0){
                var columnSCSServer = '<td style="background-color:green"><a href="#"><div id="serverscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></td>'
            }else if(data.countData.scs.server.ok == 0 && data.countData.scs.server.nok > 0){
                var columnSCSServer = '<td style="background-color:red"><a href="#"><div id="serverscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.server.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.server.ok > 0 && data.countData.scs.server.nok > 0){
                var columnSCSServer = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.server.nok+' Problem</div></a></td>'
            }
            if(data.countData.scs.switch.ok == 0 && data.countData.scs.switch.nok == 0){
                var columnSCSSwitch = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.switch.ok > 0 && data.countData.scs.switch.nok == 0){
                var columnSCSSwitch = '<td style="background-color:green"><a href="#"><div id="switchscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.scs.switch.ok == 0 && data.countData.scs.switch.nok > 0){
                var columnSCSSwitch = '<td style="background-color:red"><a href="#"><div id="switchscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.switch.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.switch.ok > 0 && data.countData.scs.switch.nok > 0){
                var columnSCSSwitch = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.switch.nok+' Problem</div></a></td>'
            }
            if(data.countData.scs.ups.ok == 0 && data.countData.scs.ups.nok == 0){
                var columnSCSups = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.scs.ups.ok > 0 && data.countData.scs.ups.nok == 0){
                var columnSCSups = '<td style="background-color:green"><a href="#"><div id="upsscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.scs.ups.ok == 0 && data.countData.scs.ups.nok > 0){
                var columnSCSups = '<td style="background-color:grey"><a href="#"><div id="upsscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.ups.nok+' Problem</div></a></td>'
            }else if(data.countData.scs.ups.ok > 0 && data.countData.scs.ups.nok > 0){
                var columnSCSups = '<td style="background-color:red"><a href="#"><div id="acscs" name="rosuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.scs.switch.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.ac.ok == 0 && data.countData.ttc.ac.nok == 0){
                var columnttcAC = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.ac.ok > 0 && data.countData.ttc.ac.nok == 0){
                var columnttcAC = '<td style="background-color:green"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.ac.ok == 0 && data.countData.ttc.ac.nok > 0){
                var columnttcAC = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.ac.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.ac.ok > 0 && data.countData.ttc.ac.nok > 0){
                var columnttcAC = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.ac.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.cctv.ok == 0 && data.countData.ttc.cctv.nok == 0){
                var columnttcCCTV = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.cctv.ok > 0 && data.countData.ttc.cctv.nok == 0){
                var columnttcCCTV = '<td style="background-color:green"><a href="#"><div id="cctvttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.cctv.ok == 0 && data.countData.ttc.cctv.nok > 0){
                var columnttcCCTV = '<td style="background-color:grey"><a href="#"><div id="cctvttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.cctv.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.cctv.ok > 0 && data.countData.ttc.cctv.nok > 0){
                var columnttcCCTV = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.cctv.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.router.ok == 0 && data.countData.ttc.router.nok == 0){
                var columnttcRouter = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.router.ok > 0 && data.countData.ttc.router.nok == 0){
                var columnttcRouter = '<td style="background-color:green"><a href="#"><div id="routerttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.router.ok == 0 && data.countData.ttc.router.nok > 0){
                var columnttcRouter = '<td style="background-color:grey"><a href="#"><div id="routerttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.router.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.router.ok > 0 && data.countData.ttc.router.nok > 0){
                var columnttcRouter = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.router.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.server.ok == 0 && data.countData.ttc.server.nok == 0){
                var columnttcServer = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.server.ok > 0 && data.countData.ttc.server.nok == 0){
                var columnttcServer = '<td style="background-color:green"><a href="#"><div id="serverttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.server.ok == 0 && data.countData.ttc.server.nok > 0){
                var columnttcServer = '<td style="background-color:grey"><a href="#"><div id="serverttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.server.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.server.ok > 0 && data.countData.ttc.server.nok > 0){
                var columnttcServer = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.server.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.switch.ok == 0 && data.countData.ttc.switch.nok == 0){
                var columnttcSwitch = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.switch.ok > 0 && data.countData.ttc.switch.nok == 0){
                var columnttcSwitch = '<td style="background-color:green"><a href="#"><div id="switchttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.switch.ok == 0 && data.countData.ttc.switch.nok > 0){
                var columnttcSwitch = '<td style="background-color:grey"><a href="#"><div id="switchttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.switch.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.switch.ok > 0 && data.countData.ttc.switch.nok > 0){
                var columnttcSwitch = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.switch.nok+' Problem</div></a></td>'
            }
            if(data.countData.ttc.ups.ok == 0 && data.countData.ttc.ups.nok == 0){
                var columnttcups = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.ttc.ups.ok > 0 && data.countData.ttc.ups.nok == 0){
                var columnttcups = '<td style="background-color:green"><a href="#"><div id="upsttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.ttc.ups.ok == 0 && data.countData.ttc.ups.nok > 0){
                var columnttcups = '<td style="background-color:grey"><a href="#"><div id="upsttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.ups.nok+' Problem</div></a></td>'
            }else if(data.countData.ttc.ups.ok > 0 && data.countData.ttc.ups.nok > 0){
                var columnttcups = '<td style="background-color:red"><a href="#"><div id="acttc" name="ttcsuta" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.ttc.ups.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.ac.ok == 0 && data.countData.tasik.ac.nok == 0){
                var columntasikAC = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.ac.ok > 0 && data.countData.tasik.ac.nok == 0){
                var columntasikAC = '<td style="background-color:green"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.ac.ok == 0 && data.countData.tasik.ac.nok > 0){
                var columntasikAC = '<td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.ac.nok+' Problem</div></a></td>'
            }else if(data.countData.tasik.ac.ok > 0 && data.countData.tasik.ac.nok > 0){
                var columntasikAC = '<td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.ac.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.cctv.ok == 0 && data.countData.tasik.cctv.nok == 0){
                var columntasikCCTV = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.cctv.ok > 0 && data.countData.tasik.cctv.nok == 0){
                var columntasikCCTV = '<td style="background-color:green"><a href="#"><div id="cctvtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.cctv.ok == 0 && data.countData.tasik.cctv.nok > 0){
                var columntasikCCTV = '<td style="background-color:grey"><a href="#"><div id="cctvtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.cctv.nok+' Problem</div></a></td>'
            }else if(data.countData.tasik.cctv.ok > 0 && data.countData.tasik.cctv.nok > 0){
                var columntasikCCTV = '<td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.cctv.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.router.ok == 0 && data.countData.tasik.router.nok == 0){
                var columntasikRouter = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.router.ok > 0 && data.countData.tasik.router.nok == 0){
                var columntasikRouter = '<td style="background-color:green"><a href="#"><div id="routertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.router.ok == 0 && data.countData.tasik.router.nok > 0){
                var columntasikRouter = '<td style="background-color:grey"><a href="#"><div id="routertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.router.nok+' Problem</div></a></td>'
            }else if(data.countData.tasik.router.ok > 0 && data.countData.tasik.router.nok > 0){
                var columntasikRouter = '<td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.router.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.server.ok == 0 && data.countData.tasik.server.nok == 0){
                var columntasikServer = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.server.ok > 0 && data.countData.tasik.server.nok == 0){
                var columntasikServer = '<td style="background-color:green"><a href="#"><div id="servertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.server.ok == 0 && data.countData.tasik.server.nok > 0){
                var columntasikServer = '<td style="background-color:grey"><a href="#"><div id="servertasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.server.nok+' Problem</div></a></td>'
            }else if(data.countData.tasik.server.ok > 0 && data.countData.tasik.server.nok > 0){
                var columntasikServer = '<td style="background-color:red"><a href="#"><div id="actasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.server.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.switch.ok == 0 && data.countData.tasik.switch.nok == 0){
                var columntasikSwitch = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.switch.ok > 0 && data.countData.tasik.switch.nok == 0){
                var columntasikSwitch = '<td style="background-color:green"><a href="#"><div id="switchtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.switch.ok == 0 && data.countData.tasik.switch.nok > 0){
                var columntasikSwitch = '<td style="background-color:grey"><a href="#"><div id="switchtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.switch.nok+' Problem</div></a></td>'
            }
            if(data.countData.tasik.ups.ok == 0 && data.countData.tasik.ups.nok == 0){
                var columntasikups = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.tasik.ups.ok > 0 && data.countData.tasik.ups.nok == 0){
                var columntasikups = '<td style="background-color:green"><a href="#"><div id="upstasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.tasik.ups.ok == 0 && data.countData.tasik.ups.nok > 0){
                var columntasikups = '<td style="background-color:grey"><a href="#"><div id="upstasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.ups.nok+' Problem</div></a></td>'
            }else if(data.countData.tasik.ups.ok == 0 && data.countData.tasik.ups.nok > 0){
                var columntasikups = '<td style="background-color:grey"><a href="#"><div id="switchtasik" name="tasik" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.tasik.ups.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.ac.ok == 0 && data.countData.cirebon.ac.nok == 0){
                var columncirebonAC = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.ac.ok > 0 && data.countData.cirebon.ac.nok == 0){
                var columncirebonAC = '<td style="background-color:green"><a href="#"><div id="actasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.ac.ok == 0 && data.countData.cirebon.ac.nok > 0){
                var columncirebonAC = '<td style="background-color:red"><a href="#"><div id="actasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.ac.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.ac.ok > 0 && data.countData.cirebon.ac.nok > 0){
                var columncirebonAC = '<td style="background-color:red"><a href="#"><div id="actasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.ac.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.cctv.ok == 0 && data.countData.cirebon.cctv.nok == 0){
                var columncirebonCCTV = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.cctv.ok > 0 && data.countData.cirebon.cctv.nok == 0){
                var columncirebonCCTV = '<td style="background-color:green"><a href="#"><div id="cctvtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.cctv.ok == 0 && data.countData.cirebon.cctv.nok > 0){
                var columncirebonCCTV = '<td style="background-color:grey"><a href="#"><div id="cctvtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.cctv.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.cctv.ok > 0 && data.countData.cirebon.cctv.nok > 0){
                var columncirebonCCTV = '<td style="background-color:grey"><a href="#"><div id="cctvtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.cctv.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.router.ok == 0 && data.countData.cirebon.router.nok == 0){
                var columncirebonRouter = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.router.ok > 0 && data.countData.cirebon.router.nok == 0){
                var columncirebonRouter = '<td style="background-color:green"><a href="#"><div id="routertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.router.ok == 0 && data.countData.cirebon.router.nok > 0){
                var columncirebonRouter = '<td style="background-color:grey"><a href="#"><div id="routertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.router.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.router.ok > 0 && data.countData.cirebon.router.nok > 0){
                var columncirebonRouter = '<td style="background-color:grey"><a href="#"><div id="routertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.router.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.server.ok == 0 && data.countData.cirebon.server.nok == 0){
                var columncirebonServer = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.server.ok > 0 && data.countData.cirebon.server.nok == 0){
                var columncirebonServer = '<td style="background-color:green"><a href="#"><div id="servertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.server.ok == 0 && data.countData.cirebon.server.nok > 0){
                var columncirebonServer = '<td style="background-color:grey"><a href="#"><div id="servertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.server.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.server.ok > 0 && data.countData.cirebon.server.nok > 0){
                var columncirebonServer = '<td style="background-color:grey"><a href="#"><div id="servertasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.server.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.switch.ok == 0 && data.countData.cirebon.switch.nok == 0){
                var columncirebonSwitch = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.switch.ok > 0 && data.countData.cirebon.switch.nok == 0){
                var columncirebonSwitch = '<td style="background-color:green"><a href="#"><div id="switchtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.switch.ok == 0 && data.countData.cirebon.switch.nok > 0){
                var columncirebonSwitch = '<td style="background-color:grey"><a href="#"><div id="switchtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.switch.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.switch.ok > 0 && data.countData.cirebon.switch.nok > 0){
                var columncirebonSwitch = '<td style="background-color:grey"><a href="#"><div id="switchtasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.switch.nok+' Problem</div></a></td>'
            }
            if(data.countData.cirebon.ups.ok == 0 && data.countData.cirebon.ups.nok == 0){
                var columncirebonups = '<td style="background-color:grey"><div style="height:100%;width:100%;text-align: center;color: white">N/A</div></td>'
            }else if(data.countData.cirebon.ups.ok > 0 && data.countData.cirebon.ups.nok == 0){
                var columncirebonups = '<td style="background-color:green"><a href="#"><div id="upstasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">OK</div></a></td>'
            }else if(data.countData.cirebon.ups.ok == 0 && data.countData.cirebon.ups.nok > 0){
                var columncirebonups = '<td style="background-color:grey"><a href="#"><div id="upstasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.ups.nok+' Problem</div></a></td>'
            }else if(data.countData.cirebon.ups.ok > 0 && data.countData.cirebon.ups.nok > 0){
                var columncirebonups = '<td style="background-color:grey"><a href="#"><div id="upstasik" name="cirebon" style="height:100%;width:100%;text-align: center;color: white">'+data.countData.cirebon.ups.nok+' Problem</div></a></td>'
            }
            $('#initialstart').remove()
            $('#bodytable').append('<tr><td>Regional SCS Jabar</td><td style="text-align:center">Subiyantoro</td>'+columnSCSAC+columnSCSCCTV+columnSCSRouter+columnSCSServer+columnSCSSwitch+columnSCSups+'</tr><tr><td>TTC Regional Jabar</td><td style="text-align:center">Teja Yuda Pratama</td>'+columnttcAC+columnttcCCTV+columnttcRouter+columnttcServer+columnttcSwitch+columnttcups+'</tr><tr><td>Branch Tasikmalaya</td><td style="text-align:center">Asep Eriyanto</td>'+columntasikAC+columntasikCCTV+columntasikRouter+columntasikServer+columntasikSwitch+columntasikups+'</tr><tr><td>Branch Cirebon</td><td style="text-align:center">Agus Nugraha</td>'+columncirebonAC+columncirebonCCTV+columncirebonRouter+columncirebonServer+columncirebonSwitch+columncirebonups+'</tr>')

            // Info Button AC
            $('#acscs').on('click', function(){
                var filterSite = $('#acscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#acttc').on('click', function(){
                var filterSite = $('#acttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#actasik').on('click', function(){
                var filterSite = $('#actasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#accirebon').on('click', function(){
                var filterSite = $('#accirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })

            //Info Button CCTV
            $('#cctvscs').on('click', function(){
                var filterSite = $('#cctvscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#cctvttc').on('click', function(){
                var filterSite = $('#cctvttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#cctvtasik').on('click', function(){
                var filterSite = $('#cctvtasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#cctvcirebon').on('click', function(){
                var filterSite = $('#cctvcirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })

            // Info Button Router
            $('#routerscs').on('click', function(){
                var filterSite = $('#routerscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#routerttc').on('click', function(){
                var filterSite = $('#routerttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#routertasik').on('click', function(){
                var filterSite = $('#routertasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#routercirebon').on('click', function(){
                var filterSite = $('#routercirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })

            // Info Button Server
            $('#serverscs').on('click', function(){
                var filterSite = $('#serverscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#serverttc').on('click', function(){
                var filterSite = $('#serverttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#servertasik').on('click', function(){
                var filterSite = $('#servertasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#servercirebon').on('click', function(){
                var filterSite = $('#servercirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })

            // Info Button Switch
            $('#switchscs').on('click', function(){
                var filterSite = $('#switchscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#switchttc').on('click', function(){
                var filterSite = $('#switchttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#switchtasik').on('click', function(){
                var filterSite = $('#switchtasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#switchcirebon').on('click', function(){
                var filterSite = $('#switchcirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })

            // Info Button UPS
            $('#upsscs').on('click', function(){
                var filterSite = $('#upsscs').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#upsttc').on('click', function(){
                var filterSite = $('#upsttc').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#upstasik').on('click', function(){
                var filterSite = $('#upstasik').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
            $('#upscirebon').on('click', function(){
                var filterSite = $('#upscirebon').attr('name');
                var filterDate = $('#datefilter').val();
                $('#datatabeldetails').empty();
                $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
                $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
                    $.map(data, function(val, i){
                        var test = val.data;
                        if(val.data.ping == 0){
                            var status = 'NOT OK'
                        }else{
                            var status = 'OK'
                        }
                        console.log(test)
                        if(val.data.optional == undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }else if(val.data.optional != undefined && val.status == 0){
                            $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional != undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                        }else if(val.data.optional == undefined && val.status != 0){
                            $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                        }
                    })
                    $('#tabledetails').dataTable({
                        "ordering": false
                    });
                })
                $('#showdata').modal('show');
            })
        })
    })
    // Info Button AC
    $('#acscs').on('click', function(){
        var filterSite = $('#acscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#acttc').on('click', function(){
        var filterSite = $('#acttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#actasik').on('click', function(){
        var filterSite = $('#actasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#accirebon').on('click', function(){
        var filterSite = $('#accirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Humadity</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ac&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.humadity+'&#176; C</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })

    //Info Button CCTV
    $('#cctvscs').on('click', function(){
        var filterSite = $('#cctvscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#cctvttc').on('click', function(){
        var filterSite = $('#cctvttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#cctvtasik').on('click', function(){
        var filterSite = $('#cctvtasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#cctvcirebon').on('click', function(){
        var filterSite = $('#cctvcirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=cctv&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })

    // Info Button Router
    $('#routerscs').on('click', function(){
        var filterSite = $('#routerscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#routerttc').on('click', function(){
        var filterSite = $('#routerttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#routertasik').on('click', function(){
        var filterSite = $('#routertasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#routercirebon').on('click', function(){
        var filterSite = $('#routercirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=router&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })

    // Info Button Server
    $('#serverscs').on('click', function(){
        var filterSite = $('#serverscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#serverttc').on('click', function(){
        var filterSite = $('#serverttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#servertasik').on('click', function(){
        var filterSite = $('#servertasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#servercirebon').on('click', function(){
        var filterSite = $('#servercirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=server&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })

    // Info Button Switch
    $('#switchscs').on('click', function(){
        var filterSite = $('#switchscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#switchttc').on('click', function(){
        var filterSite = $('#switchttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#switchtasik').on('click', function(){
        var filterSite = $('#switchtasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#switchcirebon').on('click', function(){
        var filterSite = $('#switchcirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Ping Status</th><th style="text-align:center">Temperature</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=switch&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td>'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+status+'</td><td style="text-align:center">'+val.data.temperature+'&#176; C</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })

    // Info Button UPS
    $('#upsscs').on('click', function(){
        var filterSite = $('#upsscs').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#upsttc').on('click', function(){
        var filterSite = $('#upsttc').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#upstasik').on('click', function(){
        var filterSite = $('#upstasik').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
    $('#upscirebon').on('click', function(){
        var filterSite = $('#upscirebon').attr('name');
        var filterDate = $('#datefilter').val();
        $('#datatabeldetails').empty();
        $('#datatabeldetails').append('<table id="tabledetails" class="table table-bordered"><thead><tr><th style="text-align:center">NE</th><th style="text-align:center">Phase R</th><th style="text-align:center">Phase S</th><th style="text-align:center">Phase T</th><th style="text-align:center">Info</th></tr></thead><tbody id="bodydetailstable"></tbody></table>')
        $.get('/api/report/details/table?type=ups&site='+filterSite+'&date='+filterDate, function(data, error) {    
            $.map(data, function(val, i){
                var test = val.data;
                if(val.data.ping == 0){
                    var status = 'NOT OK'
                }else{
                    var status = 'OK'
                }
                console.log(test)
                if(val.data.optional == undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }else if(val.data.optional != undefined && val.status == 0){
                    $('#bodydetailstable').append('<tr style="background-color:red;color:white;"><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional != undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center">'+val.data.optional+'</td></tr>')
                }else if(val.data.optional == undefined && val.status != 0){
                    $('#bodydetailstable').append('<tr><td>'+val.ne+'</td><td style="text-align:center">'+val.data.r+' V</td><td style="text-align:center">'+val.data.s+' V</td><td style="text-align:center">'+val.data.t+' V</td><td style="text-align:center"> - </td></tr>')
                }
            })
            $('#tabledetails').dataTable({
                "ordering": false
            });
        })
        $('#showdata').modal('show');
    })
</script>
@endsection