<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    @yield('styles')
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/mycustom.css')}}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red layout-top-nav">
    <div class="wrapper">
        <header class="main-header">
            <nav class="navbar navbar-static-top">
              <div class="container">
                <div class="navbar-header">
                  <a href="/" class="navbar-brand"><b>IT</b> Jabar</a>
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                  </button>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                  <ul class="nav navbar-nav">
                    <li><a href="/"><i class="fa fa-tachometer"></i> HOME</a></li>
                    @if(Auth::user()->role == 'admin')
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-check"></i> CHECKLIST APP <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="/checklist/items"><i class="fa fa-wrench"></i> MANAGE ITEM</a></li>
                          <li><a href="/checklist/users"><i class="fa fa-users"></i> MANAGE USERS</a></li>
                          <li><a href="/checklist/details/daily"><i class="fa fa-book"></i> REPORT</a></li>
                          <li><a href="/checklist/reports"><i class="fa fa-leanpub"></i> SUMMARY</a></li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-database"></i> DATA POTENSI <span class="caret"></span></a>
                        <ul class="dropdown-menu multi-level">
                          <li><a href="/dapot/asset/it"><i class="fa fa-folder"></i> ASSET IT</a></li>
                          <li><a href="/dapot/asset/topologi"><i class="fa fa-linode"></i> TOPOLOGI IT</a></li>
                          <li><a href="/dapot/asset/layout"><i class="fa fa-map"></i> LAYOUT IT</a></li>
                          <li class="dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-building">ASSET TTC</i></a>
                            <ul class="dropdown-menu">
                              <li><a href="/dapot/asset/ttc/profile">TTC Profile</a></li>
                              <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Building Security</i></a>
                                <ul class="dropdown-menu">
                                  <li><a href="/dapot/asset/ttc/security/cctv">CCTV</a></li>
                                  <li><a href="/dapot/asset/ttc/security/alarmdoor">ALARM DOOR</a></li>
                                  <li><a href="/dapot/asset/ttc/security/accessdoor">ACCESS DOOR</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cooling System</i></a>
                                <ul class="dropdown-menu">
                                  <li><a href="/dapot/asset/ttc/cooling/ac">AC</a></li>
                                  <li><a href="/dapot/asset/ttc/cooling/exhaust">Exhaust</a></li>
                                  <li><a href="/dapot/asset/ttc/cooling/fan">Fan</a></li>
                                  <li><a href="/dapot/asset/ttc/cooling/waterl">Water Leak</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fire Suppression System</i></a>
                                <ul class="dropdown-menu">
                                  <li><a href="/dapot/asset/ttc/fss/fe">Fire Extinguisher</a></li>
                                  <li><a href="/dapot/asset/ttc/fss/fm200">FM200</a></li>
                                  <li><a href="/dapot/asset/ttc/fss/hydrant">Hydrant</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Power</i></a>
                                <ul class="dropdown-menu">
                                  <li><a href="/dapot/asset/ttc/power/acpdb">ACPDB</a></li>
                                  <li><a href="/dapot/asset/ttc/power/battery">Battery</a></li>
                                  <li><a href="/dapot/asset/ttc/power/dcpdb">DCPDB</a></li>
                                  <li><a href="/dapot/asset/ttc/power/dcpdbfeed">DCPDB Feed</a></li>
                                  <li><a href="/dapot/asset/ttc/power/genset">Genset</a></li>
                                  <li><a href="/dapot/asset/ttc/power/inverter">Inverter</a></li>
                                  <li><a href="/dapot/asset/ttc/power/pdu">PDU</a></li>
                                  <li><a href="/dapot/asset/ttc/power/rectifier">Rectifier</a></li>
                                  <li><a href="/dapot/asset/ttc/power/riser">Riser</a></li>
                                  <li><a href="/dapot/asset/ttc/power/bbm">Tank BBM</a></li>
                                  <li><a href="/dapot/asset/ttc/power/tob">TOB</a></li>
                                  <li><a href="/dapot/asset/ttc/power/trafopln">Trafo PLN</a></li>
                                  <li><a href="/dapot/asset/ttc/power/ups">UPS</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                    @else
                      <li></li>
                    @endif
                </div>
                <div class="navbar-custom-menu">
                  <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">{{ strtoupper(Auth::user()->name) }}</span>
                      </a>
                      <ul class="dropdown-menu">
                        <li>
                          <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                          </form>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <div class="container">
                @yield('body')
            </div>
        </div>
    </div>


<script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
@yield('scripts')
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{ asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{ asset('dist/js/demo.js')}}"></script>
</body>
</html>