@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
@endsection

@section('title')
    IT Jabar
@endsection

@section('body')
    <section class="content-header">
        <h1>
            <i class="fa fa-tachometer"></i> DASHBOARD
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-leanpub"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">REPORTS</span>
                        <span class="info-box-number">100</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection