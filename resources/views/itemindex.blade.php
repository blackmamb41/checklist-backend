@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title')
    MANAGE ITEMS
@endsection

@section('body')
    <section class="content-header">
        <h1>
            <i class="fa fa-wrench"></i> MANAGE ITEMS
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Items Network Element</h3>
                        <a href="/checklist/items/add"><button type="button" class="btn btn-block btn-success" style="width: 15%; margin-top: 2%"><i class="fa fa-plus"></i> Create New NE</button></a>
                    </div>
                    <div class="box-body">
                        <table id="itemstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Network Element</th>
                                    <th>Site</th>
                                    <th>Floor</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                    <tr>
                                        <th>{{$item->codename}}</th>
                                        <th>{{$item->NE}}</th>
                                        @if($item->site == 'rosuta')
                                            <th>Regional Office Jabar</th>
                                        @elseif($item->site == 'ttcsuta')
                                            <th>TTC Regional Jabar</th>
                                        @elseif($item->site == 'cirebon')
                                            <th>Branch Cirebon</th>
                                        @elseif($item->site == 'tasik')
                                            <th>Branch Tasikmalaya</th>
                                        @elseif($item->site == 'cianjur')
                                            <th>Grapari Cianjur</th>
                                        @elseif($item->site == 'windu')
                                            <th>Branch Windu</th>
                                        @elseif($item->site == 'soreang')
                                            <th>Branch Soreang</th>
                                        @elseif($item->site == 'grapdago')
                                            <th>Grapari Dago</th>
                                        @endif
                                        <th>{{$item->floor}}</th>
                                        <th style="text-align: center;"><a href="/checklist/items/edit?codename={{$item->codename}}"><i class="fa fa-pencil" style="color: blue"></i></a>&nbsp;<a href="/checklist/items/delete?codename={{$item->codename}}"><i class="fa fa-trash" style="color: red;"></i></a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#itemstable').DataTable({})
        })
    </script>
@endsection