@extends('core')

@section('styles')
@endsection

@section('title', 'CREATE NEW NE')

@section('body')
    <div class="row justify-content-md-center">
        <section class="content-header">
            <h1>
                Add NE Item
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <form action="/checklist/items/add/create" method="POST">
                    @csrf
                    <div class="col-md-8">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">NE Details</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name"><i class="fa fa-certificate" style="color: red;"> </i>Hardware</label>
                                    <input type="text" class="form-control" id="ne" name="ne" placeholder="Hardware Name" required/>
                                </div>
                                <div class="form-group">
                                    <label for="serial">Serial Number :</label>
                                    <input type="text" class="form-control" id="sn" name="sn" placeholder="Serial Number Hardware" />
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type"><i class="fa fa-certificate" style="color: red;"> </i>Type :</label>
                                        <select name="type" id="type" required class="form-control">
                                            <option value="">Choose Type..</option>
                                            <option value="ups">UPS</option>
                                            <option value="switch">Switch</option>
                                            <option value="router">Router</option>
                                            <option value="server">Server</option>
                                            <option value="ac">AC</option>
                                            <option value="cctv">CCTV</option>
                                            <option value="wireless">Wireless</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ts"><i class="fa fa-certificate" style="color: red;"> </i>Assignment :</label>
                                        <select name="enginer" id="enginer" class="form-control" required>
                                            <option value="">Choose TS...</option>
                                            @foreach($user as $ts)
                                                <option value="{{ $ts->id }}">{{ $ts->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="site"><i class="fa fa-certificate" style="color: red;"> </i>Site :</label>
                                        <select name="site" id="site" class="form-control" required>
                                            <option value="">Choose Site.....</option>
                                            <option value="rosuta">Regional Office Jabar</option>
                                            <option value="ttcsuta">TTC Regional Jabar</option>
                                            <option value="cirebon">Branch Cirebon</option>
                                            <option value="tasik">Branch Tasikmalaya</option>
                                            <option value="cianjur">Grapari Cianjur</option>
                                            <option value="windu">Branch Windu</option>
                                            <option value="soreang">Branch Soreang</option>
                                            <option value="grapdago">Grapari Dago</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="floor"><i class="fa fa-certificate" style="color: red;"> </i>Floor :</label>
                                        <select name="floor" id="floor" class="form-control" required>
                                            <option value="">Choose Floor....</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-block btn-success">Create</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
