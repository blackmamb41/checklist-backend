@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('title', 'Edit User')

@section('body')
    <section class="content-header">
        <h1>
            Add User
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <form action="/checklist/users/edit/save/{{$user->id}}" method="POST">
                @csrf
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Identitiy User</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name"><i class="fa fa-certificate" style="color: red;"></i> Name : </label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required value="{{$user->name}}"/>
                            </div>
                            <div class="form-group">
                                <label for="nik"><i class="fa fa-certificate" style="color: red;"></i> NIK : </label>
                                <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK Employee" required value="{{$user->nik}}"/>
                            </div>
                            <div class="form-group">
                                <label for="email">Email :</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail Corporation" autocomplete="email" value="{{$user->email}}">
                            </div>
                            <div id="changepass">
                                <div id="btnforchange" style="width: 40%">
                                    <button class="btn btn-block btn-primary" id="btnchange">Change Password</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Privileges</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="role">Role :</label>
                                <select class="form-control select2" name="role" id="role" style="margin-bottom: 10px;">
                                    @foreach($role as $roles)
                                        @if($user->role == $roles)
                                            <option value="{{$roles}}" selected>
                                                @if($roles == 'admin')
                                                    Administrator
                                                @else
                                                    Enginer
                                                @endif
                                            </option>
                                        @else
                                            <option value="{{$roles}}">
                                                @if($roles == 'admin')
                                                    Administrator
                                                @else
                                                    Enginer
                                                @endif
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-block btn-success">SAVE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function() {
            $('.select2').select2()
        })
        $("#btnchange").click(function() {
            $('#btnforchange').remove();
            $('#changepass').append('<label for="password">Password :</label><div class="form-group"><input type="password" class="form-control" id="password" name="password" placeholder="New Password" required"/></div>')
        })
    </script>
@endsection