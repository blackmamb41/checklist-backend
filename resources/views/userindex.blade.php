@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title')
    MANAGE USERS
@endsection

@section('body')
    <section class="content-header">
        <h1>
            <i class="fa fa-wrench"></i> MANAGE USERS
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                        <a href="/checklist/users/add"><button type="button" class="btn btn-block btn-success" style="width: 15%; margin-top: 2%"><i class="fa fa-plus"></i> Add New Users</button></a>
                    </div>
                    <div class="box-body">
                        <table id="itemstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>NIK</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $user)
                                    <tr>
                                        <th>{{$user->name}}</th>
                                        <th>{{$user->nik}}</th>
                                        <th>{{$user->email}}</th>
                                        <th>{{$user->role}}</th>
                                        <th style="text-align: center;"><a href="/checklist/users/edit/{{$user->id}}"><i class="fa fa-pencil" style="color: blue;"></i></a>&nbsp;<a href="/checklist/users/delete?id={{$user->id}}"><i class="fa fa-trash" style="color: red;"></i></a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#itemstable').DataTable({})
        })
    </script>
@endsection