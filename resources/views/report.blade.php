@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/mycustom.css')}}">
@endsection

@section('title', 'REPORTS')

@section('body')
    <section class="content-header">
        <h1>
            <i class="fa fa-leanpub"></i> REPORTS
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3>Filter :</h3>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label style="font-size: 10px;">Start Date :</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="startdate">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label style="font-size: 10px;">End Date :</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="enddate">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label style="font-size: 10px">TS :</label>
                                <select name="enginer" id="enginer" class="form-control select2">
                                    <option value="">Select TS</option>
                                    @foreach($data as $enginer)
                                        <option value="{{$enginer->id}}">{{$enginer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for=""></label>
                                <button class="btn btn-block btn-primary" id="process"><i class="fa fa-search"></i> Process</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="bodycontent">
                        <div style="text-align: center;" id="initialstart">
                            <h2>SELECT DATE AND ENGINER FIRST</h2>
                        </div>
                        <!-- <table id="reportstable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="text-align: center">NE</th>
                                    <th style="text-align: center"><i class="fa fa-check-circle-o" style="color: green"></i></th>
                                    <th style="text-align: center"><i class="fa fa-times-circle" style="color: red"></i></th>
                                    <th style="text-align: center">Sites</th>
                                    <th style="text-align: center">TS</th>
                                </tr>
                            </thead>
                            <tbody id="datareport">

                            </tbody>
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script>
        $(function() {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = mm + '/' + dd + '/' + yyyy;
            $('#startdate').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy',
                endDate: today
            })
            $('#enddate').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy',
                endDate: today
            })
        })
        $('#process').click(function() {
            var startDate = $('#startdate').val();
            var endDate = $('#enddate').val();
            var ts = $('#enginer').val();
            if(startDate == '') {
                $('#initialstart').remove();
                $('#bodycontent').append('<div style="text-align: center;" id="initialstart"><h2>PLEASE SELECT ALL FILTER FIRST</h2></div>');
            }else if(endDate == '') {
                $('#initialstart').remove();
                $('#bodycontent').append('<div style="text-align: center;" id="initialstart"><h2>PLEASE SELECT ALL FILTER FIRST</h2></div>');
            }else if(ts == '') {
                $('#initialstart').remove();
                $('#bodycontent').append('<div style="text-align: center;" id="initialstart"><h2>PLEASE SELECT ALL FILTER FIRST</h2></div>');
            }else{
                $('#reportstable_wrapper').remove();
                $('#exportexcel').remove();
                $('#initialstart').remove();
                $('#bodycontent').append('<div class="spinner" id="initialstart"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>')
                $.get('/api/summary?start='+startDate+'&end='+endDate+'&enginer='+ts, function(data, error) {
                    $('#initialstart').remove();
                    $('#bodycontent').append('<table id="reportstable" class="table table-bordered table-striped"><thead><tr><th style="text-align: center">No</th><th style="text-align: center">NE</th><th style="text-align: center">Sites</th><th style="text-align: center">OK</th><th style="text-align: center">NOK</th></tr></thead><tbody id="datareport"></tbody></table><div style="width: 30%" id="exportexcel"><a href="/reports/export?start='+startDate+'&end='+endDate+'&enginer='+ts+'"><button class="btn btn-primary btn-block" id="export">Export To Excel</button></a></div>')
                    $.map( data, function(val, i) {
                        var nomor = i+1
                        $('#datareport').append('<tr><td style="text-align: center">'+nomor+'</td><td>'+val.NE+'</td><td>'+val.site+'</td><td style="text-align: center">'+val.countOK+'</td><td style="text-align: center">'+val.countNOK+'</td></tr>')
                    })
                    $('#reportstable').dataTable({
                        "order": [[0, "asc"]],
                        "columnDefs": [{
                            "targets": [0,2,3],
                            "orderable": false,
                            "searchable": false
                        }]
                    })
                    console.log(data);
                }).fail(function() {
                    alert("error")
                })
            }
        })
    </script>
@endsection