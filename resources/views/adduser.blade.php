@extends('core')

@section('styles')
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('title', 'Add User')

@section('body')
    <section class="content-header">
        <h1>
            Add User
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <form action="/checklist/users/add/create" method="POST">
                @csrf
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Identitiy User</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name"><i class="fa fa-certificate" style="color: red;"></i> Name : </label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required/>
                            </div>
                            <div class="form-group">
                                <label for="nik"><i class="fa fa-certificate" style="color: red;"></i> NIK : </label>
                                <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK Employee" required/>
                            </div>
                            <div class="form-group">
                                <label for="email">Email :</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail Corporation" autocomplete="email">
                            </div>
                            <div class="from-group">
                                <label for="password"><i class="fa fa-certificate" style="color: red;"></i> Password : </label>
                                <input type="password" class="form-control" name="password" id="password" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Privileges</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="role">Role :</label>
                                <select class="form-control select2" name="role" id="role" style="margin-bottom: 10px;">
                                    <option value="admin">Administrator</option>
                                    <option value="enginer">Enginer</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-block btn-success">CREATE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $(function() {
            $('.select2').select2()
        })
    </script>
@endsection