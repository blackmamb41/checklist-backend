<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Network Elements</th>
            <th>Site</th>
            <th>OK</th>
            <th>NOK</th>
        </tr>
    </thead>
    <tbody>
        @foreach($report as $key=>$value)
            <tr>
                <th>{{$key+1}}</th>
                <th>{{$value->NE}}</th>
                <th>
                    @if($value->site == 'rosuta')
                        Regional Office Jabar
                    @elseif($value->site == 'ttcsuta')
                        TTC Regional Jabar
                    @elseif($value->site == 'cirebon')
                        Branch Cirebon
                    @elseif($value->site == 'tasik')
                        Branch Tasikmalaya
                    @elseif($value->site == 'cianjur')
                        Grapari Cianjur
                    @elseif($value->site == 'windu')
                        Branch Windu
                    @elseif($value->site == 'soreang')
                        Branch Soreang
                    @endif
                </th>
                <th>{{$value->countOK}}</th>
                <th>{{$value->countNOK}}</th>
            </tr>
        @endforeach
    </tbody>
</table>