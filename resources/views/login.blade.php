<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css')}}">
    <title>LOGIN DASHBOARD</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" style="background-image: url('{{ asset('img/wallpaper.jpg')}}'); background-size: 100% 100%;">
    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('img/telkomsel.png')}}" alt="logo" style="width: 20%; height: 20%;">
            <a href="#" style="color: #fff"><b style="color: #fff">IT</b> Jabar</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign First for start session</p>
            <form action="{{ route('login') }}" method="POST" aria-label="{{ __('Login') }}">
                @csrf
                <div class="form-group {{ $errors->has('nik') ? ' has-error' : '' }}">
                    <input type="number" class="form-control" placeholder="NIK Employee" required id="nik" name="nik" autofocus/>
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                    <input type="password" class="form-control" placeholder="Password" required id="password" name="password"/>
                    <span class="fa fa-key form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js')}}"></script>
<script>
$(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
});
</script>
</body>
</html>